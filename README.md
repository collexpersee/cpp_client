# Collex-Persée Plugin for client
Version **1.0** | April, 18th 2019
## QGIS plugin for Qgis 2.18
The QGIS Plugin cpp_client allow user to interact with [cpp_node](https://gitlab.com/collexpersee/cpp_server) and [cpp_server](https://gitlab.com/collexpersee/cpp_node). By choosing parameters and filters, the plugin use an OGC standard protocol **WFS** to request and retrieve filtered data from cpp_server. Then it is directly shown on Qgis. The plugin enable interaction when clicking on new points added after a request : A slideshow containing small picture which illustrate the content can pop up.
## 1. How to install the plugin ?
All you need is to copy your plugin cpp_client folder in : `~/.qgis2/python/plugins/`

For example on Linux, **~** is located in `/home/user/`.

Don't forget to configure some variable : 
- **WFS_server** it is QGIS server address. default value is : localhost/cgi-bin
- **NODE_server** it is node address. default value is : localhost:3000 

Default values works if you have [cpp_server](https://gitlab.com/collexpersee/cpp_server) and [cpp_node](https://gitlab.com/collexpersee/cpp_node) working both on your local machine.
## 2. How to use the plugin ?
Don't hesite to see wiki [here](https://gitlab.com/collexpersee/cpp_client/wikis/Videos)
### 2.1. Request form
![Form](src/form/form.png)

On one hand, when the button on the left in QGIS plugin bar is clicked, a form is displayed. We explain how to fill this form, in the followed sections, before submitting it.
#### 2.1.1. Gazetteer
**YOU MUST** set a gazetteer. Click on the button `GET` on the right to get the gazetteers available on the node server. 
#### 2.1.2. Keywords
You could add until three keywords to filter the research. To add one, write it in the search bar and click on the button `+`.
#### 2.1.3. Select some places
There are two ways to select some places.
##### Category
It is possible to select all places of a category. If a gazetteer is selected, you could get the different categories available in this gazetteer, with the button `GET` on the right. The option `ALL` could be used to select all places.
##### Area
We could draw an area to get only the places in this area. To draw it :
- by giving a longitude, a latitude and a radius to draw a circle according to the given radius around a point.
- with a radius and the features selected in the different layers loaded in QGIS. A buffer will be built around each features.
- if you do not want to select a specific area, choose the option `Nothing`.

#### 2.1.4. Select a temporal range
For the last step, you could specify a temporal range with
two years.
### 2.2. Slide show
![Form](src/slide_show/slideshow.png)

On the other hand, the button on the right displays the results. It works in two times.
#### 2.2.1. Clicker
For the first time, the plugin waits for a mouse click on the QGIS canvas. To cancel this event, click again on the button. 
#### 2.2.2. Slide show
When a mouse click is caught, the plugin will search for the closest point stacked in the active layer. It is possible not to find any point. If a point is found, a window is opened to show all pictures matched with this point.
There is a buffer around the click, so if you click close to several points, it will load them together, and show all the pictures.
#### 2.2.3. Links
Now you are able to see small pictures, and by clicking on link, you will be redirected in the original url adress in your browser.
## 3. Limits
Note that sometimes, you need to select twice the slide_show button to make sure it works.
Note that until Persée does not provide links to JPEG images in thesaurus, this line is commented !
```
slide_show.py : l.155 : # self.lst_originals_link.extend(persee_originals)
```
**PLEASE REACTIVATE WHEN IT'S WORKING**
## 4. Licence
TO DO : to fill
> Authors : A. Fondere, C. Crapart, E. Viegas, M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski, ENSG