from src.shapefile_result import ShapefileResult


class TestShapefile(object):
    """ Tests for shapefiles """

    def __init__(self):
        self.shp = ShapefileResult()

    def test_init_from_json(self):
        """ Test to decode JSON """

        self.shp.init_from_json("{\"key\": \"value\"}")
        assert {u'key': u'value'} == self.shp.result

    def test_open_shp_qgis(self):
        pass
