from cairodata import CairoData
import urllib
from qgis.utils import iface


class CairoDataTest(object):
    """ Tests for CairoData """

    def __init__(self):
        """ Build a CairoData instance """
        self.cd = CairoData(iface)

    def test_submit(self):
        """ Test to submit the form """

        # Set the parameters
        self.cd.dlg.gazetteer.setCurrentIndex(0)
        self.cd.dlg.lst_kw.setText("")
        self.cd.dlg.lon.setValue(30.09)
        self.cd.dlg.lat.setValue(31.32)
        self.cd.dlg.radius.setValue(5)
        self.cd.dlg.where.setCurrentIndex(0)
        self.dlg.unit.currentIndex(0)

        # Submit and split the URI sent to QGIS server
        url = self.cd.submit()
        url = url.split("?")[1]
        lst = url.split("&")
        param = {}

        # Parse the URI
        for k in range(len(lst)):
            temp = param[k].split("=")
            param[temp[0]] = temp[1]
            param[temp[0]] = urllib.parse.unquote(param[temp[0]])

        # Tests for each parameter
        assert (param["TH"] == "OpenthesoHumaNum"), "th"
        assert (param["GAZETTEER"] == "idg=MT62&th=42")
        assert (param["KEYWORD"] == "")
        assert (param["DIR"] == "")
        assert (abs(float(param["LONG"]) - 30.09) < 1e-12)
        assert (abs(float(param["LAT"]) - 31.32) < 1e-12)
        assert (abs(float(param["RD"]) - 5) < 1e-12)

    def test_reset(self):
        """ Test to reset the form """

        self.cd.dlg.keyword.setText("XYZ")
        self.cd.dlg.lst_kw.setText("ABC")
        self.cd.dlg.where.setCurrentIndex(1)
        self.cd.dlg.lon.setValue(30.09)
        self.cd.dlg.lat.setValue(31.32)
        self.cd.dlg.radius.setValue(5)
        self.cd.dlg.unit.setCurrentIndex(1)
        self.cd.db_reset()

        assert self.cd.dlg.gazetteer.currentIndex() == 0
        assert self.cd.dlg.keyword.text == ""
        assert self.cd.dlg.lst_kw.text == ""
        assert self.cd.dlg.where.currentIndex() == 0

    def test_get_coord(self):
        """ Test to get the coordinates """

        self.cd.dlg.coord.setChecked(True)
        self.cd.dlg.lon.setValue(30.09)
        self.cd.dlg.lat.setValue(31.32)

        x, y = self.cd.get_coord()
        assert abs(x - 30.09) < 1e-12
        assert abs(y - 31.32) < 1e-12

        self.cd.dlg.coord.setChecked(False)
        x, y = self.cd.get_coord()
        assert x == ""
        assert y == ""

    def test_get_radius(self):
        """ Test to get the radius """

        self.cd.dlg.coord.setChecked(True)
        self.cd.dlg.selection.setChecked(True)
        self.cd.dlg.radius.setValue(5)
        self.cd.dlg.unit.setCurrentIndex(0)

        rd = self.cd.get_radius()
        assert abs(rd - 5) < 1e-12

        self.cd.dlg.selection.setChecked(False)
        rd = self.cd.get_radius()
        assert abs(rd - 5) < 1e-12

        self.cd.dlg.coord.setChecked(False)
        self.cd.dlg.selection.setChecked(True)
        rd = self.cd.get_radius()
        assert abs(rd - 5) < 1e-12

        self.cd.dlg.selection.setChecked(False)
        rd = self.cd.get_radius()
        assert rd == ""

    def test_get_category(self):
        """ Test to get the place category """

        self.cd.dlg.where.setCurrentIndex(0)
        ctg = self.cd.get_category()
        assert ctg == ""

    def test_add(self):
        """ Test to add a keyword """

        self.cd.dlg.keyword.setText("XYZ")
        self.cd.add()
        assert self.cd.dlg.lst_kw.text() == "xyz"

        self.cd.dlg.keyword.setText("xYz")
        self.cd.add()
        assert self.cd.dlg.lst_kw.text() == "xyz ; xyz"

        self.cd.dlg.keyword.setText("ab;c")
        self.cd.add()
        assert self.cd.dlg.lst_kw.text() == "xyz ; xyz ; abc"

    def test_choose_place(self):
        """ Test to initialise the combo-box to choose a place """

        self.cd.choose_place()
        assert self.cd.dlg.where.currentIndex() == 0


print("cdt")
cdt = CairoDataTest()
cdt.test_submit()
cdt.test_reset()
cdt.test_get_coord()
cdt.test_get_radius()
cdt.test_get_category()
cdt.test_add()
cdt.test_choose_place()
print("finish")
