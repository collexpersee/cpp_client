import time
from src.functions import encode_dico, conversion_dist, get_time


def test_encode_dico():
    """ Test to encode a dictionary as unicode """

    d = {"↩": ["é", "à"]}
    res = encode_dico(d)
    assert (res == {u"\u21a9": [u"\xe9", u"\xe0"]}), "Encode dictionary"


def test_conversion_dist():
    """ Test to convert a distance into kilometers """

    # Kilometer
    dist = 5
    unit = "km"
    assert (abs(conversion_dist(dist, unit) - 5) < 1e-2), "Conversion dist"

    # Mile
    dist = 3.107
    unit = "mi"
    assert (abs(conversion_dist(dist, unit) - 5) < 1e-2), "Conversion dist"

    # Nautical mile
    dist = 2.7
    unit = "nmi"
    assert (abs(conversion_dist(dist, unit) - 5) < 1e-2), "Conversion dist"

    # Meter
    dist = 5000
    unit = "m"
    assert (abs(conversion_dist(dist, unit) - 5) < 1e-2), "Conversion dist"

    # Yard
    dist = 5468.066
    unit = "yd"
    assert (abs(conversion_dist(dist, unit) - 5) < 1e-2), "Conversion dist"


def test_get_time():
    """ Test to get the current time """

    t1 = time.localtime()
    str_t = "%02d/%02d/%04d %02d:%02d:%02d" % (
        t1.tm_mday, t1.tm_mon, t1.tm_year,
        t1.tm_hour, t1.tm_min, t1.tm_sec
    )

    t2 = get_time()
    assert (t2 == str_t), "Get time"
