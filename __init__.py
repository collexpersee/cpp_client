# -*- coding: utf-8 -*-
"""
/***************************************************************************
    CairoData
                                A QGIS plugin
    to fill
                             -------------------
        begin                : 2019-03-12
        copyright            : (C) 2019 by A. Fondere, C. Crapart,
            E. Viegas, M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski
            Ecole Nationale des Sciences Géographiques
        email                : collexpersee@gmail.com
        git sha              : $Format:%H$
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load CairoData class from file CairoData.

    :param iface: A QGIS interface instance.
    :type iface: QgisInterface
    """
    #
    from .cairodata import CairoData
    return CairoData(iface)
