from qgis.core import QgsGeometry, QGis
from qgis.utils import iface


class PrepareWFS(object):
    """
    In this class, we can generate Parameter Dic for WFS request given some
    parameters. We use prepare_dic_wms() function
    """
    def __init__(self):
        self.dic = {
            'SERVICE': 'WFS',
            'REQUEST': 'GetFeature',
            'srsname': 'EPSG:4326',
            'format_options': 'CHARSET:UTF-8',
            'version': '1.0.0'
        }

    def prepare_dic_wfs(
            self, gazetteer, keyword, category, polygon, datedeb, dateend):
        """
        WORKING AT 70 %
        this function maps the parameters from qgis plugin cpp client
        and create the right parameters adapted for WFS
        TO IMPLEMENT :
            - Just the Date research is missing
            - if category is filled, it replace content of keyword variable
            - Many keywords can't work

        :param gazetteer: name of gazetteer
        :param keyword: string of keyword(s)
        :param category: string of category
        :param datedeb: value of lower bound of date range TO BE IMPLEMENTED
        :param dateend: value of upper bound of date range TO BE IMPLEMENTED
        :param polygon: WKT of polygon, it must be a polygon

        :return a dic with all parameters for working in WFS
        """

        # make sur there is no space and only '_'
        gazetteer = '_'.join(gazetteer.split(' '))
        self.dic['typeName'] = gazetteer
        # set number of applyied filters to 0
        nb_constraint = 0
        # set all filters to 0
        filter_str_poly = ''
        filter_str_keyword = ''
        filter_str_categ = ''
        filter_str_date = ''

        if polygon != '':
            # research with at least 1constraints
            nb_constraint += 1
            geom = QgsGeometry.fromWkt(polygon)

            if geom.type() == QGis.Polygon:
                poly = geom.asPolygon()
                list_of_points = ''

                for i in range(len(poly[0])):
                    if i == len(poly[0]) - 1:
                        list_of_points += '{} {}'.format(
                            poly[0][i][0],
                            poly[0][i][1]
                        )
                    else:
                        list_of_points += '{} {} '.format(
                            poly[0][i][0],
                            poly[0][i][1]
                        )

                filter_str_poly = '<Intersects><PropertyName>' + gazetteer + \
                                  '</PropertyName>' + \
                                  '<gml:Polygon><gml:exterior>' \
                                  '<gml:LinearRing><gml:posList>' + \
                                  list_of_points + \
                                  '</gml:posList></gml:LinearRing>' \
                                  '</gml:exterior></gml:Polygon></Intersects>'
            else:
                iface.messageBar().pushMessage(
                    "CPP",
                    "This isn't a polygon here,can't operate on bad geometry",
                    level=2
                )

        if keyword != '':
            # add constraint
            nb_constraint += 1
            propert2 = 'labels'
            if len(keyword.split(" ; ")) > 1:
                # we have many keywords here
                list_keywords = keyword.split(" ; ")

                if len(list_keywords) == 2:
                    filter_str_keyword = \
                        '<And><PropertyIsLike wildCard="*" singleChar="." es' \
                        'cape="!" matchCase="false"><PropertyName>' +\
                        propert2 + '</PropertyName><Literal>*' + \
                        list_keywords[0] + '*</Literal></PropertyIsLike><Pro' \
                        'pertyIsLike wildCard="*" singleChar="." escape="!" ' \
                        'matchCase="false"><PropertyName>' + propert2 + '</P' \
                        'ropertyName><Literal>*' + list_keywords[1] + '*</Li' \
                        'teral></PropertyIsLike></And>'
                else:
                    # limit to 3 keywords
                    filter_str_keyword = \
                        '<And><And><PropertyIsLike wildCard="*" singleChar="' \
                        '." escape="!" matchCase="false"><PropertyName>' +\
                        propert2 + '</PropertyName><Literal>*' +\
                        list_keywords[0] + '*</Literal></PropertyIsLike><Pro' \
                        'pertyIsLike wildCard="*" singleChar="." escape="!" ' \
                        'matchCase="false"><PropertyName>' + propert2 + '</P' \
                        'ropertyName><Literal>*' + list_keywords[1] + '*</Li' \
                        'teral></PropertyIsLike></And><PropertyIsLike wildCa' \
                        'rd="*" singleChar="." escape="!" matchCase="false">' \
                        '<PropertyName>' + propert2 + '</PropertyName><Liter' \
                        'al>*' + list_keywords[2] + '*</Literal></PropertyIs' \
                        'Like></And>'

            else:
                filter_str_keyword =\
                    '<PropertyIsLike wildCard="*" singleChar="." escape="!" ' \
                    'matchCase="false"><PropertyName>' + propert2 + '</Prope' \
                    'rtyName><Literal>*' + keyword + '*</Literal></PropertyI' \
                    'sLike>'

        if category != '':
            # add constraint
            nb_constraint += 1
            propert3 = 'ancestors'
            filter_str_categ =\
                '<PropertyIsLike wildCard="*" singleChar="." escape="!">'\
                '<PropertyName>' + propert3 + '</PropertyName>' + \
                '<Literal>*' + category + '*</Literal></PropertyIsLike>'

        if datedeb != '' and dateend != '':
            # add constraint
            nb_constraint += 1
            propert_field_date_deb = 'first_date'
            propert_field_date_fin = 'last_date'
            filter_str_date =\
                '<Or><And><PropertyIsLessThanOrEqualTo><PropertyName>' + \
                propert_field_date_deb + '</PropertyName><ogc:Literal>' + \
                datedeb + '</ogc:Literal></PropertyIsLessThanOrEqualTo>'\
                '<PropertyIsGreaterThanOrEqualTo><PropertyName>' + \
                propert_field_date_fin + '</PropertyName><ogc:Literal>' + \
                datedeb + '</ogc:Literal></PropertyIsGreaterThanOrEqualTo>'\
                '</And><And><PropertyIsGreaterThanOrEqualTo><PropertyName>' + \
                propert_field_date_deb + '</PropertyName><ogc:Literal>' + \
                datedeb + '</ogc:Literal></PropertyIsGreaterThanOrEqualTo>'\
                '<PropertyIsLessThanOrEqualTo><PropertyName>' + \
                propert_field_date_deb + '</PropertyName><ogc:Literal>' + \
                dateend + '</ogc:Literal></PropertyIsLessThanOrEqualTo>'\
                '</And></Or>'

        if nb_constraint != 0:
            if nb_constraint == 1:
                # just one filter is set
                self.dic['FILTER'] =\
                    '<Filter>' + filter_str_poly +\
                    filter_str_keyword + filter_str_categ + filter_str_date +\
                    '</Filter>'

            elif nb_constraint == 2:
                # just detect what is the combination and what filter are
                # activated
                if filter_str_categ == '':
                    # the other are activated
                    self.dic['FILTER'] =\
                        '<Filter><And>' + filter_str_poly +\
                        filter_str_keyword + filter_str_date +\
                        '</And></Filter>'

                else:
                    if filter_str_poly == '':
                        # the other are activated
                        self.dic['FILTER'] =\
                            '<Filter><And>' + filter_str_keyword + \
                            filter_str_categ + filter_str_date +\
                            '</And></Filter>'
                    else:
                        self.dic['FILTER'] =\
                            '<Filter><And>' + filter_str_poly + \
                            filter_str_categ + filter_str_date +\
                            '</And></Filter>'

            elif nb_constraint == 3:
                # add three filters
                if filter_str_categ == '':
                    # filter on poly, date and keyword
                    self.dic['FILTER'] = '<Filter><And><And>' +\
                                         filter_str_poly + filter_str_date +\
                                         '</And>' + filter_str_keyword +\
                                         '</And></Filter>'

                elif filter_str_date == '':
                    # filter on poly category and keyword
                    self.dic['FILTER'] =\
                        '<Filter><And><And>' + filter_str_poly + \
                        filter_str_categ + '</And>' +\
                        filter_str_keyword + '</And></Filter>'

                elif filter_str_keyword == '':
                    # filter on poly category and date
                    self.dic['FILTER'] =\
                        '<Filter><And><And>' + filter_str_poly +\
                        filter_str_date + '</And>' + filter_str_categ +\
                        '</And></Filter>'

                else:
                    # filter on category date and keyword
                    self.dic['FILTER'] =\
                        '<Filter><And><And>' + filter_str_keyword +\
                        filter_str_date + '</And>' + filter_str_categ +\
                        '</And></Filter>'

            elif nb_constraint == 4:
                # add all filters
                self.dic['FILTER'] =\
                    '<Filter><And><And><And>' + filter_str_keyword + \
                    filter_str_date + '</And>' + filter_str_categ +\
                    '</And>' + filter_str_poly + '</And></Filter>'

            # make ensure good encoding, without space caracter
            # self.dic['FILTER'] = "+".join(self.dic['FILTER'].split(" "))
            return self.dic

        return self.dic
