import urllib
import os
from qgis.core import QgsApplication
from qgis.utils import iface


class PictureResult(object):
    """
    This class can donwload image from node server

    CONFIGURATION URL IS SET IN URL PREFIX, maybe you need to change

    It retrieves picture links from a list and downloads through node
    """

    def __init__(self):
        """ Build a PictureResult instance """

        self.url = ''
        self.url_prefix = 'http://localhost:3000/GetPicture?'

    # Future function, not still developed
    def download_all(self):
        """ Retrieve all data from all places """
        pass

    # reset img cache before running a WFS request
    def reset_cache(self):
        folder_image = QgsApplication.qgisSettingsDirPath()[:-1] + \
            'python/plugins/cpp_client/cache'
        if os.path.exists(folder_image):
            if os.path.exists(folder_image + '/img_bnf'):
                files = os.listdir(folder_image + '/img_bnf')
                for i in range(0, len(files)):
                    os.remove(folder_image + '/img_bnf/' + files[i])

            if os.path.exists(folder_image + '/img_persee'):
                files = os.listdir(folder_image + '/img_persee')
                for i in range(0, len(files)):
                    os.remove(folder_image + '/img_persee/' + files[i])

    def download_a_place(self, list_bnf, list_persee):
        """
        Download all pictures matched with a place

        :param list_bnf: List of small pictures from BNF
        :type list_bnf:  Path list

        :param list_persee: List of small pictures from Persee
        :type list_persee:  Path list
        """

        # first create the folder a the right location
        folder_img_rel = QgsApplication.qgisSettingsDirPath()[:-1] + \
            'python/plugins/cpp_client/'
        folder_image = QgsApplication.qgisSettingsDirPath()[:-1] + \
            'python/plugins/cpp_client/cache'

        # Make sure the folders exist
        if not os.path.exists(folder_image):
            os.mkdir(folder_image)
        if not os.path.exists(folder_image + '/img_bnf'):
            os.mkdir(folder_image + '/img_bnf')
        if not os.path.exists(folder_image + '/img_persee'):
            os.mkdir(folder_image + '/img_persee')

        # Ask Node.js and verify if all pictures is in the cache
        for elem in list_bnf:
            # verify it is not in folder already
            if not os.path.isfile(folder_img_rel + elem[2:]):
                img = urllib.request.urlopen(
                    self.url_prefix + 'name=' + elem
                ).read()

                if img == '':
                    iface.messageBar().pushMessage(
                        "CPP",
                        "No picture in the server",
                        level=1
                    )

                try:
                    with open(folder_img_rel + elem[2:], "w") as file:
                        file.write(img)
                except Exception:
                    iface.messageBar().pushMessage(
                        "CPP",
                        "Error writing file, make sure you have rights",
                        level=2
                    )

        # Ask Node.js and verify if all pictures is in the cache
        for elem in list_persee:
            # verify it is not in folder already
            if not os.path.isfile(folder_img_rel + elem[2:]):
                img = urllib.request.urlopen(
                    self.url_prefix + 'name=' + elem
                ).read()

                if img == '':
                    iface.messageBar().pushMessage(
                        "CPP",
                        "No picture in the server",
                        level=1
                    )

                try:
                    with open(folder_img_rel + elem[2:], "w") as file:
                        file.write(img)
                except Exception:
                    iface.messageBar().pushMessage(
                        "CPP",
                        "Error writing file, make sure you have rights",
                        level=2
                    )
