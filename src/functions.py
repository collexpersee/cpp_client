# -*- coding: utf-8 -*-
from qgis.core import QgsMapLayerRegistry, QgsPoint, QgsGeometry,\
    QgsCoordinateTransform, QgsCoordinateReferenceSystem as QRefSys, QgsFeature
from datetime import datetime as dt
from qgis.utils import iface


# Colors for the table
COLOR0 = "\"background-color: rgb(108, 172, 228)\""
COLOR1 = "\"background-color: rgb(212, 217, 221)\""

LOGS = ".qgis2/python/plugins/cpp_client/other_files/logs.txt"
TR1 = QgsCoordinateTransform(QRefSys(4326), QRefSys(3857))
TR2 = QgsCoordinateTransform(QRefSys(3857), QRefSys(4326))


# ============== BUFFERS ==============
def select(rd):
    """
    Export each feature selected in QGIS canvas to WKT

    :param rd: Buffer radius
    :type rd:  Float, greater or equal than 0

    :return: List of WKT strings separated by a space.
    :rtype:  String as '<WKT> ... <WKT>'
    """

    layers = QgsMapLayerRegistry.instance().mapLayers().values()
    selection = []
    lst = []

    # Get selected features in all vector layers
    for l in layers:
        # Vector layer -> get the selected features
        try:
            for ft in l.selectedFeatures():
                selection.append(ft)

        # Raster layer -> skip
        except AttributeError:
            continue

    # Export each feature to WKT format
    for sel in selection:
        lst.append(compute_buffer(sel, rd))

    # Merge the different WKT with ' '
    return " ".join(lst)


def lat_long(rd, lat, long):
    """
    Export a buffer around a point as WKT

    :param rd: Buffer radius
    :type rd:  Float, greater or equal than 0

    :param lat: Latitude of a point
    :type lat:  Float angular (°)

    :param long: Longitude of a point
    :type long:  Float angular (°)

    :return: Buffer as WKT
    :rtype:  String
    """

    if long != "" and lat != "":
        ft = QgsFeature()
        geom = QgsGeometry().fromPoint(QgsPoint(float(long), float(lat)))
        ft.setGeometry(geom)
        return compute_buffer(ft, rd)


def transform_polygon(geom, tr):
    """
    Transform a polygon into a projection.

    :param geom: Polygon to transform
    :type geom:  QgsGeometry as a polygon

    :param tr: Transformation object
    :type tr:  QgsCoordinateTransform

    :return: New polygon
    :rtype:  QgsGeometry as a polygon
    """

    lst = geom.asPolygon()[0]  # 0 --> exterior ring

    for k in range(len(lst)):
        lst[k] = tr.transform(lst[k])

    return QgsGeometry().fromPolygon([lst])


def transform_polyline(geom):
    """
    Transform a polyline into a projection.

    :param geom: Polyline to transform
    :type geom:  QgsGeometry as a polyline

    :return: New polyline
    :rtype:  QgsGeometry as a polyline
    """

    lst = geom.asPolyline()

    for k in range(len(lst)):
        lst[k] = TR1.transform(lst[k])

    return QgsGeometry().fromPolyline(lst)


def compute_buffer(ft, rd):
    """
    Compute the buffer and export as WKT

    :param ft: Feature, for which a buffer is computed
    :type ft:  QgsFeature

    :param rd: Buffer radius
    :type rd:  Float, greater or equal than 0

    :return: Buffer exported as WKT
    :rtype:  String as WKT
    """

    geom = ft.geometry()
    t = geom.type()

    if t == 0:
        # Point
        point = TR1.transform(geom.asPoint())
        buffer = QgsGeometry().fromPoint(point).buffer(1000 * rd, 10)

    elif t == 1:
        # Polyline
        buffer = transform_polyline(geom).buffer(1000 * rd, 10)

    else:
        # Polygon
        buffer = transform_polygon(geom, TR1).buffer(1000 * rd, 10)

    return transform_polygon(buffer, TR2).exportToWkt(precision=5)


# ============== DISTANCE =============
def conversion_dist(dist, unit="km"):
    """
    Convert a distance into kilometers

    :param dist: Distance to convert
    :type dist:  Float

    :param unit: Unit of this value
    :type unit:  String among this list : {km, m, mi, nmi, yd}

    :return: Distance converted into kilometers
    :rtype:  Float in kilometers
    """

    if unit == "mi":
        dist *= 1.609344
    elif unit == "nmi":
        dist *= 1.852
    elif unit == "m":
        dist /= 1000.0
    elif unit == "yd":
        dist *= 0.0009144

    return int(1e5 * dist) / 1e5


# ================ LOGS ===============
def get_time():
    """
    Get the current time
    :return: Current time with this format DD/MM/YYYY HH:MM:SS
    :rtype:  String
    """

    return dt.now().strftime("%d/%m/%Y %H:%M:%S")


def write_log(service, param, msg):
    """
    Write a new log into the file 'logs.txt'

    :param service: Service used
    :type service:  CamelCase string (e.g. : 'oneServiceByExample')

    :param param: Different parameters of this URL
    :type param:  Dictionary, in which the keys are the parameters

    :param msg: Response message
    :type msg:  Message describing the response of the request
    """

    keys = sorted(param.keys())
    param["Response"] = msg
    keys.append("Response")
    str_param = "\n\t".join(["%s = %s" % (k, param[k]) for k in keys])

    with open(LOGS, "a") as f:
        f.write("%s : %s\n\t%s\n\n" % (get_time(), service, str_param))


def display_log():
    """
    Display the logs in the form (tab 'Logs')
    :return: Content t display in the tab 'logs'
    :rtype:  HTML string
    """

    with open(LOGS) as f:
        cont = f.read()

    logs = cont.split("\n\n")
    logs.reverse()

    if len(logs) > 0:
        logs.pop(0)

    format_log(logs)
    return "\n".join(logs)


def format_log(logs):
    """
    Write a log with a HTML format
    :param logs: Logs to format
    :type logs:  String list
    """

    style = "style=\"color: red\""

    for k in range(len(logs)):
        lines = logs[k].split("\n")
        date = lines[0].split(" : ")[0]
        service = lines[0].split(" : ")[1]
        temp = []

        # Build the table with the key-value couple
        for l in range(1, len(lines)):
            kv = lines[l][1:].split(" = ")
            key = kv[0].capitalize()
            val = kv[1].replace(" ; ", "\n").replace(";", "\n")

            if l % 2 == 1:
                color = COLOR0
            else:
                color = COLOR1

            temp.append("<tr style=%s><td>%s</td><td>%s</td></tr>" % (
                color, key, val)
            )

        # Assemble the different section to display
        logs[k] = "<h3><span %s>%s</span>\t%s</h3><table>%s</table>" % (
            style, date, service, "".join(temp)
        )


# =============== LAYER ===============
def get_layer():
    """
    Get the feature iterator of the active layer in QGIS

    :return: Feature list of active layer
    :rtype:  Feature iterator
    :except: If the active layer is a raster layer
    """

    try:
        return iface.activeLayer().getFeatures()
    except AttributeError:
        iface.messageBar().pushMessage(
            "CPP",
            "QGIS Raster layer selected",
            level=2
        )
        return
