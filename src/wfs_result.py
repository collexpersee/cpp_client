from qgis.core import QgsVectorLayer, QgsMapLayerRegistry


class WFSResult(object):
    """
    in this class we retrieve (formatted WFS)URL and create WFS layers
    in Qgis browser
    """
    def __init__(self):
        pass

    def add_to_map(self, str_url, gazetteer):
        """
        Open WFS directly in qgis

        :param str_url: URL to send to QGIS server
        :type str_url:  URL as WFS format (cf. prepare_wfs.py)

        :param gazetteer: Gazetteer name, which will be the name of result
            layer in QGIS
        :type gazetteer:  String
        """

        # is there a response in the result
        vl = QgsVectorLayer(str_url, gazetteer, 'WFS')
        nbr = vl.featureCount()

        if not vl.isValid():
            return "Layer failed to load !", 2

        QgsMapLayerRegistry.instance().addMapLayer(vl)
        return "WFS layer added with %d features" % nbr, 3
