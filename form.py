# -*- coding: utf-8 -*-
from src.functions import select, lat_long, conversion_dist, write_log, \
    display_log
import urllib
import json
from src.prepare_wfs import PrepareWFS
from src.picture_result import PictureResult
from src.wfs_result import WFSResult
from src.form.form_dialog import FormDialog
from qgis.utils import iface


BACK = unicode("↩", "utf-8")
NODE = "http://localhost:3000"
WFS = "http://localhost/cgi-bin/"


class Form(object):
    def __init__(self, plugin):
        """
        Initialise and show the request form
        :param plugin: Plugin
        :type plugin:  CairoData instance
        """

        self.pl = plugin
        self.th = None
        self.stack = []
        self.keywords = []

        # Show the dialog
        self.pl.dlg = FormDialog()
        self.setup()
        self.pl.dlg.show()

        # See if OK was pressed
        if self.pl.dlg.exec_():
            self.submit()

    def submit(self):
        """
        Sends an URL to QGIS Server according to the user information,
        when the form is submitted
        """

        # Build the URL to send to QGIS server
        wfs = self.get_param()

        # here there will be a specific variable to have theso name
        url_wfs_pt2 = 'opentheso' + '/qgis_mapserv.fcgi?'
        url_wfs_pt3 = urllib.unquote_plus(urllib.urlencode(wfs))

        wfs_result = WFSResult()
        # we add to request to create a WFS layer in QGIS and retrieve message
        mess, lvl = wfs_result.add_to_map(
            WFS + url_wfs_pt2 + url_wfs_pt3,
            wfs['typeName']
        )
        # delete cache before run
        PictureResult().reset_cache()
        # Write the logs
        write_log("wfs", wfs, mess)
        self.pl.dlg.log.setText(display_log())

        # Message bar
        iface.messageBar().pushMessage("CPP", mess, level=lvl)

    def setup(self):
        """ Sets up the user interface and adds the different events """

        self.pl.dlg.reset.clicked.connect(self.reset)
        self.pl.dlg.add.clicked.connect(self.add)
        self.pl.dlg.when.clicked.connect(self.switch_date)

        # Add the different gazetteers
        self.pl.dlg.get_g.clicked.connect(self.get_gazetteer)
        self.pl.dlg.gazetteer.clear()

        # Where
        self.pl.dlg.coord.clicked.connect(self.switch_where)
        self.pl.dlg.selection.clicked.connect(self.switch_where)
        self.pl.dlg.nothing.clicked.connect(self.switch_where)
        self.pl.dlg.unit.addItems(["km", "mi", "nmi", "m", "yd"])
        self.pl.dlg.lon.setRange(-180, 180)
        self.pl.dlg.lon.setDecimals(5)
        self.pl.dlg.lat.setRange(-90, 90)
        self.pl.dlg.lat.setDecimals(5)
        self.pl.dlg.radius.setRange(0, 1e7)
        self.pl.dlg.radius.setDecimals(3)
        self.pl.dlg.where.clear()

        self.pl.dlg.get_c.setEnabled(False)
        self.pl.dlg.get_c.clicked.connect(self.choose_place)

        # New module
        self.pl.dlg.db_reset.clicked.connect(self.db_reset)
        self.pl.dlg.db_add.clicked.connect(self.db_add)

        # Logs
        self.pl.dlg.log.setText(display_log())

    def reset(self):
        """ Reset the whole form """

        self.pl.dlg.gazetteer.setCurrentIndex(0)
        self.pl.dlg.keyword.setText("")
        self.pl.dlg.lst_kw.setText("")
        self.keywords = []
        self.pl.dlg.add.setEnabled(True)
        self.pl.dlg.where.clear()
        self.pl.dlg.lon.setValue(0)
        self.pl.dlg.lat.setValue(0)
        self.pl.dlg.radius.setValue(0)
        self.pl.dlg.unit.setCurrentIndex(0)
        self.pl.dlg.db_name.setText("http://")

    def get_coord(self):
        """
        Get the coordinates from the form
        :return: A couple of coordinates (longitude, latitude).
            No coordinates --> ("", "")
        """

        # Fill the place according to the option selected
        if self.pl.dlg.coord.isChecked():
            return self.pl.dlg.lon.text(), self.pl.dlg.lat.text()
        else:
            return "", ""

    def get_radius(self):
        """
        Get the buffer radius from the form
        :return: Radius converted into kilometers
        """

        if self.pl.dlg.coord.isChecked() or self.pl.dlg.selection.isChecked():
            return conversion_dist(
                float(self.pl.dlg.radius.text()),
                self.pl.dlg.unit.currentText()
            )

        else:
            return ""

    def get_category(self):
        """
        Get the category chosen by the user
        :return: Category without the word 'ALL' """

        return self.pl.dlg.where.currentText() \
            .replace("ALL ", "").replace("ALL", "")

    def get_param(self):
        """
        Get the parameters from the form
        :return: Two dictionaries,
            - first one for node server
            - second one for QGIS server adpated for WFS request
        """

        self.add()
        thesaurus = self.pl.dlg.gazetteer.currentText()
        keywords = self.pl.dlg.lst_kw.text()
        longitude, latitude = self.get_coord()
        radius = self.get_radius()
        category = self.get_category()
        dict_wfs = PrepareWFS()

        if self.pl.dlg.selection.isChecked():
            poly = select(radius)
        elif self.pl.dlg.coord.isChecked():
            poly = lat_long(radius, latitude, longitude)
        else:
            poly = ""

        if self.pl.dlg.when.isChecked():
            date_from = self.pl.dlg.dt_from.text()
            date_to = self.pl.dlg.dt_to.text()
        else:
            date_from = ""
            date_to = ""

        return dict_wfs.prepare_dic_wfs(
            thesaurus, keywords, category, poly, date_from, date_to
        )

    def get_gazetteer(self):
        """ Get the gazetteers from the server """

        self.pl.dlg.gazetteer.clear()
        url = "%s/getThesos" % NODE
        res = json.loads(urllib.request.urlopen(url).read())

        for th in res:
            self.pl.dlg.gazetteer.addItem(th)

        write_log("getTheso", {}, "Get %d thesaurus" % len(res))
        self.pl.dlg.log.setText(display_log())
        self.pl.dlg.get_c.setEnabled(len(res) > 0)

    def switch_where(self):
        """ Select the coordinates and/or ome features on QGIS. """

        if self.pl.dlg.coord.isChecked():
            # Select the option 'coordinates'
            self.pl.dlg.lat.setEnabled(True)
            self.pl.dlg.lon.setEnabled(True)
            self.pl.dlg.radius.setEnabled(True)
            self.pl.dlg.unit.setEnabled(True)

        elif self.pl.dlg.selection.isChecked():
            # Select the option 'selection'
            self.pl.dlg.lat.setEnabled(False)
            self.pl.dlg.lon.setEnabled(False)
            self.pl.dlg.radius.setEnabled(True)
            self.pl.dlg.unit.setEnabled(True)

        else:
            # No option selected
            self.pl.dlg.lat.setEnabled(False)
            self.pl.dlg.lon.setEnabled(False)
            self.pl.dlg.radius.setEnabled(False)
            self.pl.dlg.unit.setEnabled(False)

    def add(self):
        """ Add a keyword to the list. The keyword number is limited to 3 """

        if self.pl.dlg.keyword.text() != "" and len(self.keywords) < 3:
            self.keywords.append(self.pl.dlg.keyword.text().lower()
                                 .replace(";", ""))
            self.pl.dlg.lst_kw.setText(" ; ".join(self.keywords))
            self.pl.dlg.keyword.setText("")

        if len(self.keywords) > 2:
            self.pl.dlg.add.setEnabled(False)

    def db_add(self):
        """ Add a gazetteer and send the new URL to the server """

        param = {"link": self.pl.dlg.db_name.text()}
        url = "%s/addTheso?%s" % (NODE, urllib.urlencode(param))
        res = urllib.request.urlopen(url).read()

        if res == "Downloaded":
            self.pl.dlg.gazetteer.addItem(self.pl.dlg.db_name.text())

        write_log("addTheso", param, res)
        self.db_reset()

    def db_reset(self):
        """ Reset the form part used to add a new database """

        self.pl.dlg.db_name.setText("http://")

    def choose_place(self):
        """ Sets the combo box to select a place """

        # Get the categories
        param = {"theso": self.pl.dlg.gazetteer.currentText()}
        url = "%s/getCategories?%s" % (NODE, urllib.urlencode(param))
        res = urllib.request.urlopen(url).read()
        self.th = json.loads(res)
        write_log(
            "getCategories", param,
            "%d category(ies)" % len(self.th.keys())
        )

        # Initialise the browsing stack ; self.th['root'] is the first node
        self.stack = ["root"]
        self.pl.dlg.where.clear()      # Clear the combo box
        self.pl.dlg.where.addItems(["ALL"] + sorted(self.th["root"]))

        # Browse to another folder
        self.pl.dlg.where.currentIndexChanged.connect(self.browse)

    def browse(self):
        """ Browse to a child or to the parent """

        # Go back to the parent
        if self.pl.dlg.where.currentText() == BACK:
            self.go_back()

        # Go to one of children
        else:
            self.browse_children(self.pl.dlg.where.currentText())

    def go_back(self):
        """ Go back to the parent """

        # Back to the previous state
        self.stack.pop()
        parent = self.stack[-1]
        self.pl.dlg.where.clear()

        # No back button when the cursor is on the root
        if len(self.stack) > 1:
            # Not the root
            self.pl.dlg.where.addItem("ALL %s" % parent)
            self.pl.dlg.where.addItem(BACK)
        else:
            # Root node
            self.pl.dlg.where.addItem("ALL")

        # Display the items of the parent
        self.pl.dlg.where.addItems(sorted(self.th[parent]))

        # Set the index and show the list
        self.pl.dlg.where.setCurrentIndex(0)
        self.pl.dlg.where.showPopup()

    def browse_children(self, name):
        """ Go to one child """

        # Nothing to do if the item does not exist (e.g. "ALL")
        if name in self.th:
            children = self.th[name]
        else:
            return None

        # Check if this child is a node or a leaf
        if len(children) > 0:
            # Node
            self.stack.append(name)  # Browse to 'name' node
            self.pl.dlg.where.clear()  # Clear the list

            # Display the items of the new child
            self.pl.dlg.where.addItems(
                ["ALL %s" % name, BACK] + sorted(children)
            )

            # Set the index and show the list
            self.pl.dlg.where.setCurrentIndex(0)
            self.pl.dlg.where.showPopup()
        # Leaf -> nothing to do

    def switch_date(self):
        """ Enable or hide the date form """

        checked = self.pl.dlg.when.isChecked()
        self.pl.dlg.lb_from.setEnabled(checked)
        self.pl.dlg.dt_from.setEnabled(checked)
        self.pl.dlg.lb_to.setEnabled(checked)
        self.pl.dlg.dt_to.setEnabled(checked)
