# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CairoData
                                 A QGIS plugin
 to do
                              -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4.QtCore import QRect, Qt
from PyQt4.QtGui import QLabel, QPixmap
from qgis.core import QgsCoordinateTransform, QgsCoordinateReferenceSystem,\
    QgsGeometry, QgsPoint
from qgis.utils import iface
from src.picture_result import PictureResult
from src.slide_show.slide_show_dialog import SlideShowDialog
from src.functions import get_layer
from ast import literal_eval
from qgis.core import QgsApplication


TR1 = QgsCoordinateTransform(
    QgsCoordinateReferenceSystem(4326),
    QgsCoordinateReferenceSystem(3857)
)


class SlideShow(object):
    def __init__(self, plugin):
        """
        Initialise and show the request form
        :param plugin: Plugin
        :type plugin:  CairoData instance
        """

        self.pl = plugin
        self.lst = []
        self.lst_originals_link = []
        self.selected_name = ''

        # Show the dialog
        self.pl.dlg = SlideShowDialog()
        self.pl.dlg.img = []
        self.pl.dlg.names = []
        self.pl.dlg.scroll.actionTriggered.connect(self.move)

    def set(self):
        """ Set the list of images """

        self.pl.dlg = SlideShowDialog()
        self.pl.dlg.img = []
        self.pl.dlg.names = []

        for k in range(len(self.lst)):
            temp = QLabel(self.pl.dlg)
            temp.setGeometry(QRect(5 + 110 * k, 10, 100, 100))
            temp.setAlignment(Qt.AlignCenter)
            temp.setObjectName(self.selected_name + "img%s" % k)
            self.pl.dlg.img.append(temp)

            href = "<a href=\"%s\">Image %d</a>" % (
                self.lst_originals_link[k], k
            )

            # activate link to picture
            temp = QLabel(self.pl.dlg)
            temp.setGeometry(QRect(5 + 110 * k, 80, 100, 100))
            temp.setOpenExternalLinks(True)
            temp.setAlignment(Qt.AlignCenter)
            temp.setObjectName(self.selected_name + "legend%s" % k)
            temp.setText(href)
            self.pl.dlg.names.append(temp)

        self.display()
        self.pl.dlg.scroll.setEnabled(len(self.lst) > 7)

    def display(self):
        """ Display an image on the central label """
        abs_path = QgsApplication.qgisSettingsDirPath()[:-1] + \
            'python/plugins/cpp_client/'

        for k in range(len(self.lst)):
            pixel_map = QPixmap(abs_path + self.lst[k][2:]) \
                .scaled(100, 100, Qt.KeepAspectRatio)
            self.pl.dlg.img[k].setPixmap(pixel_map)

    def move(self):
        """ Move the pictures when the slider is triggered """

        val = self.pl.dlg.scroll.value() / 0.99
        dx = 5 - 1.1 * val * (len(self.lst) - 7)

        for k in range(len(self.lst)):
            x = dx + 110 * k
            self.pl.dlg.img[k].setGeometry(QRect(x, 10, 100, 100))

    def clicker(self, point, button):
        """
        Catch an event on QGIS canvas and find the images matched
        with this point

        :param point: Point, which is clicked by the mouse
        :type point:  QEvent

        :param button: unused
        :type button:  Button
        """

        ft_iter = get_layer()
        if ft_iter is None:
            return

        x, y = point.x(), point.y()
        geom = QgsGeometry().fromPoint(TR1.transform(QgsPoint(x, y)))
        rd = self.pl.iface.mapCanvas().scale() / 200
        self.lst = []
        self.lst_originals_link = []

        for ft in ft_iter:
            tmp = TR1.transform(ft.geometry().asPoint())
            buffer = QgsGeometry().fromPoint(tmp).buffer(rd, 26)

            if buffer.contains(geom):
                try:
                    # we use here the small picture that are generated already
                    # in the server
                    bnf_links = literal_eval(
                        ft.attribute("small_bnf_links"))
                    persee_links = literal_eval(ft.attribute(
                        "small_persee_links"))

                    # we use here the original picture link to give as link
                    # for showing slide show
                    bnf_originals = literal_eval(
                        ft.attribute("bnf_links"))
                    # persee_originals = literal_eval(
                    #     ft.attribute("persee_links"))

                    # self.lst_originals_link.extend(persee_originals)
                    self.lst_originals_link.extend(bnf_originals)

                    PictureResult().download_a_place(bnf_links, persee_links)
                    self.lst.extend(persee_links)
                    self.lst.extend(bnf_links)
                    self.selected_name += ft.attribute("name")
                    # add to slide show now with this new list

                except KeyError:
                    iface.messageBar().pushMessage(
                        "CPP",
                        "This layer does not contain the good attributes",
                        level=2
                    )
                    return

        iface.messageBar().pushMessage(
            "CPP",
            "There are %d picture(s)" % len(self.lst),
            level=3
        )

        if len(self.lst) > 0:
            self.set()
            self.pl.dlg.show()
